const express = require('express');
const app = express();
const port = 3000;

const mylib = require('./mylib');
/**
 * 
 * @param {Number} a 
 * @param {Number} b 
 * @returns {Number} Sum of a and p
 */
const add = (a,b) => {
    return a+ b;
};



// Endpoint localhost:3000/
app.get('/', (req, res) => {
    //const sum = add(1,2);
    //console.log(sum);
    res.send('Hello world');
});

// Endpoint localhost:3000/add?a=42&b=21 
app.get('/add', (req, res) => {
    const a =parseInt(req.query.a);
    const b =parseInt(req.query.b);

    const total = mylib.sum(a,b);
    console.log({a,b});
    res.send('add works' + total.toString())
})


app.listen(3000, () => {
    console.log(`example listening to port ${port}`);
});
