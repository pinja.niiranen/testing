const expect = require('chai').expect;
const assert = require('chai').assert;
const mylib = require('../src/mylib')
const should = require('chai').should();

describe('Unit testing mylib.js', () => {

    let myvar =undefined;
    before(() => {
        myvar = 1;
    })
    
    it('SHould return 2 when using sum function with a=1 and b=1',
    () => {
        const result = mylib.sum(1,1)
        expect(result).to.equal(2)
    })

    it.skip('assert foo is not bar', () => {
        assert('foo' !== 'bar')
    })

    it('Myvar should exist',
    () => {
        console.log('world')
        should.exist(myvar)
    })

    after(() => {
        console.log('hello')
    })
})